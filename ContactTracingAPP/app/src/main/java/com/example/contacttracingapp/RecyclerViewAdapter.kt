package com.example.contacttracingapp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.contacttracingapp.databinding.ItemAnswerBinding

class RecyclerViewAdapter(
    private val list: List<String>
    ) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = ItemAnswerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolder(binding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.binding.textView.text = list[position]

        }

        override fun getItemCount() = list.size

        class ViewHolder(val binding: ItemAnswerBinding) : RecyclerView.ViewHolder(binding.root)
    }
