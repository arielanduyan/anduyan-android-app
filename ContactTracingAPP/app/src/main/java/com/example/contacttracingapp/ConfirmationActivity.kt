package com.example.contacttracingapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.EditText
import com.example.contacttracingapp.databinding.ActivityConfirmationBinding


class ConfirmationActivity: AppCompatActivity() {

    private lateinit var binding: ActivityConfirmationBinding

    private var listTasks: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        //ViewBinding
        binding = ActivityConfirmationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()

    }

    private fun init() {
        setUpRecyclerView()

        val button = findViewById<Button>(R.id.button3)
        button.setOnClickListener{
            val intent = Intent(this,SuccessActivity::class.java)
            startActivity(intent);

        }
    }

    private fun setUpRecyclerView() {
        val fullname = intent.getStringExtra("fullname")
        val mobile = intent.getStringExtra("mobile")
        val address = intent.getStringExtra("address")
        val ans1 = intent.getStringExtra("ans1")
        val ans2 = intent.getStringExtra("ans2")

        listTasks.add(fullname.toString())
        listTasks.add(mobile.toString())
        listTasks.add(address.toString())
        listTasks.add(ans1.toString())
        listTasks.add(ans2.toString())

        binding.recyclerView1.apply {
            adapter = RecyclerViewAdapter(list = listTasks)
            layoutManager = GridLayoutManager(context,  1, RecyclerView.VERTICAL, false)
        }
    }
}