package com.example.contacttracingapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


class HealthActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_info)

        val fname = intent.getStringExtra("fullname")
        val mobile = intent.getStringExtra("mobile")
        val address = intent.getStringExtra("address")

        val button = findViewById<Button>(R.id.button2)
        button.setOnClickListener{
            val intents = Intent(applicationContext,ConfirmationActivity::class.java)
            val ans1 = findViewById<EditText>(R.id.answer1)
            val ans2 = findViewById<EditText>(R.id.answer2)

            intents.putExtra("fullname", fname)
            intents.putExtra("mobile", mobile)
            intents.putExtra("address", address)
            intents.putExtra("ans1", ans1.text.toString())
            intents.putExtra("ans2", ans2.text.toString())
            startActivity(intents);
        }
    }
}