package com.example.contacttracingapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.example.contacttracingapp.databinding.ActivityMainBinding


class MainActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button1)
        button.setOnClickListener{
            val intents = Intent(applicationContext,HealthActivity::class.java)
            val name = findViewById<EditText>(R.id.name)
            val mobile = findViewById<EditText>(R.id.mobile)
            val address = findViewById<EditText>(R.id.address)

            intents.putExtra("fullname", name.text.toString())
            intents.putExtra("mobile", mobile.text.toString())
            intents.putExtra("address", address.text.toString())
            startActivity(intents);

        }
    }
}